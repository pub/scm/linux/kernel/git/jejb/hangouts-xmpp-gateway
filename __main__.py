import sys
sys.path.insert(0, './lib/hangups')
sys.path.insert(0, './lib/sleekxmpp')
import os
import logging
import xmppthread

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO,
                        format='%(levelname)-8s %(message)s')

    s = xmppthread.XMPPThread('google.hangouts', '153.66.160.254')

    if s.connect():
        s.process(block=True)
    sys.exit(0)

